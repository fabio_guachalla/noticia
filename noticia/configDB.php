<?php

G::LoadClass('case');
G::LoadClass('configuration');
G::loadClass('pmFunctions');

function loadTitle()
{
    $query = 'select * from PMT_NOTICIA';
    $table = executeQuery($query);
    $cont = 0;
    $dataV;
    foreach ($table as $record) {
        $dataV[$cont] = $record['TITLE'];
        $cont++;
    }
    return $dataV;
}

function loadContent()
{
    $query = 'select * from PMT_NOTICIA';
    $table = executeQuery($query);
    $cont = 0;
    $dataV;
    foreach ($table as $record) {
        $dataV[$cont] = $record['CONTENT'];
        $cont++;
    }
    return $dataV;
}

function loadImage()
{
    $query = 'select * from PMT_NOTICIA';
    $table = executeQuery($query);
    $cont = 0;
    $dataV;
    foreach ($table as $record) {
        $dataV[$cont] = $record['IMAGE'];
        $cont = $cont + 1;
    }
    return $dataV;
}
