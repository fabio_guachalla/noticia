{config_load file="test.conf" section="setup"}
{include file="header.tpl"}
<body role="document">
<div class = "container">
<div class="text-center"><h1>{$name}</h1></div>

{section name=outer loop=$titulo}
    <div class="panel panel-primary col-lg-4">
        <div class="panel-heading">
            <h3 class="panel-title">{$titulo[outer]}</h3>
        </div>
        <div class="panel-body">
            <a href="#" class="thumbnail"><img src="{$image[outer]}" alt="{$image[outer]}" class="img-responsive"></a>
            {$contenido[outer]}
        </div>
    </div>

{/section}

</div>
{include file="footer.tpl"}
