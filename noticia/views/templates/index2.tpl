{config_load file="test.conf" section="setup"}
{include file="header.tpl"}
<body role="document">
<div class = "container">
<div class="text-center" id="title"><h1>{$name}</h1></div>
<div class="text-right">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  Create News
</button>
</div>
<table class="table table-hover">
    <tr>
        <td>Title</td>
        <td>Content</td>
        <td>Image</td>
    </tr>

    {section name=outer loop=$titulo}
    <tr>
        <td>{$titulo[outer]}</td>
        <td>{$contenido[outer]}</td>
        <td><img src="{$image[outer]}" alt="{$image[outer]}" class="img-responsive"></td>
        <td id="edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></td>
        <td id="delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></td>
    </tr>
    {/section}

</table>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">New News</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Title:</label>
            <input type="text" class="form-control" id="title">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Content:</label>
            <textarea class="form-control" id="Content"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>

</div>

</div>
{include file="footer.tpl"}
