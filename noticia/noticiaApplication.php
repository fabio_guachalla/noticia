<?php
/**
 * welcome.php for plugin noticia.
 */
require 'configUser.php';
require 'configDB.php';

$vt = loadTitle();
$vc = loadContent();
$vi = loadImage();

$smarty->assign('title', 'Noticias', true);
$smarty->assign('name', 'NOTICIAS', true);
$smarty->assign('titulo', $vt);
$smarty->assign('contenido', $vc);
$smarty->assign('image', $vi);

$smarty->display('index.tpl');
