<?php
G::LoadClass('SMARTY');

$smarty = new Smarty();

$smarty->template_dir = PATH_PLUGINS.'noticia/views/templates/';
$smarty->compile_dir = PATH_SMARTY_C;
$smarty->cache_dir = PATH_SMARTY_CACHE;
$smarty->config_dir = PATH_PLUGINS.'noticia/views/configs';
