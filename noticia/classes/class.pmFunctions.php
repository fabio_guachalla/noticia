<?php
/**
 * class.noticia.pmFunctions.php
 *
 * ProcessMaker Open Source Edition
 * Copyright (C) 2004 - 2008 Colosa Inc.
 * *
 */

////////////////////////////////////////////////////
// noticia PM Functions
//
// Copyright (C) 2007 COLOSA
//
// License: LGPL, see LICENSE
////////////////////////////////////////////////////

function noticia_getMyCurrentDate()
{
	return G::CurDate('Y-m-d');
}

function noticia_getMyCurrentTime()
{
	return G::CurDate('H:i:s');
}
